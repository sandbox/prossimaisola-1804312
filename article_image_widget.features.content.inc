<?php

/**
 * Implementation of hook_content_default_fields().
 */
function article_image_widget_content_default_fields() {
  $fields = array();

  // Exported field: field_image2
  $fields['articolo-field_image2'] = array(
    'field_name' => 'field_image2',
    'type_name' => 'articolo',
    'display_settings' => array(
      'weight' => '15',
      'parent' => 'group_homepage',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_plain' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_html' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '0',
    'multiple' => '1',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'argomenti_immagini' => 'argomenti_immagini',
      'articolo' => 0,
      'associazione' => 0,
      'book' => 0,
      'casetracker_basic_case' => 0,
      'domanda' => 0,
      'faq' => 0,
      'event' => 0,
      'feed' => 0,
      'feed_item' => 0,
      'group' => 0,
      'home_categoria' => 0,
      'image' => 0,
      'fattispecie_particolare' => 0,
      'landingpage' => 0,
      'blog' => 0,
      'simplenews' => 0,
      'newsfeed' => 0,
      'page' => 0,
      'panel' => 0,
      'profile' => 0,
      'casetracker_basic_project' => 0,
      'richiesta' => 0,
      'risposta_associazione' => 0,
      'scheda_associazione' => 0,
      'scheda_approfondimento' => 0,
      'scheda_didattica' => 0,
      'scheda_problemi' => 0,
      'home_statica' => 0,
      'feed_ical_item' => 0,
      'feed_ical' => 0,
    ),
    'advanced_view' => 'image_widget',
    'advanced_view_args' => '',
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'nid' => NULL,
          '_error_element' => 'default_value_widget][field_image2][0][nid][nid',
          'actions' => array(
            'browse' => 'Browse',
            'remove' => 'Remove',
          ),
        ),
      ),
      'default_value_php' => NULL,
      'create_ref' => 0,
      'content_preview' => '--',
      'content_preview_position' => 'below',
      'dialog_title' => 'immagine Explorer',
      'theme_css' => '<module>',
      'dialog_api' => 'modalframe',
      'dialog_dialogClass' => '',
      'dialog_bgiframe@boolean' => '0',
      'dialog_width@int' => '800',
      'dialog_height@int' => '600',
      'dialog_position' => 'center',
      'dialog_minWidth@int' => '',
      'dialog_minHeight@int' => '',
      'dialog_maxWidth@int' => '',
      'dialog_maxHeight@int' => '',
      'dialog_draggable@boolean' => '0',
      'dialog_resizable@boolean' => '0',
      'dialog_show' => 'null',
      'dialog_hide' => 'null',
      'label' => 'immagine',
      'weight' => '15',
      'description' => '',
      'type' => 'nodereference_explorer',
      'module' => 'nodereference_explorer',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('immagine');

  return $fields;
}
